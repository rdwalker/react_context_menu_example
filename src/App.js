import React, {Component} from 'react';
import './App.css';


const ContextMenuChoice = (props) => (
    <div
        className={'menuChoice'}
        onClick={props.handleClick}
    >
        {props.text}
    </div>
);


const ContextMenu = (props) => {
    return props.contextMenuOpen ?
        <div
            className={'contextMenu'}
            style={{
                position: 'absolute',
                left: props.coords.x || 0,
                top: props.coords.y || 0,
            }}

        >
            <ContextMenuChoice text={'Choice 1'} handleClick={() => alert('Clicked Choice 1')}/>
            <ContextMenuChoice text={'Choice 2'} handleClick={() => alert('Clicked Choice 2')}/>
            <ContextMenuChoice text={'Choice 3'} handleClick={() => alert('Clicked Choice 3')}/>
        </div> : null
};


class App extends Component {

    state = {
        contextMenuOpen: false,
        contextMenuCoords: {},
    };

    componentDidMount(){
        // Listen for key presses
        window.onkeyup = (eventData) => {
            this.closeContextMenu(eventData);
        }
    }

    // Open the menu when right-click and get coords of where the user clicked
    openContextMenu = (eventData) => {
        eventData.preventDefault();
        eventData.persist();
        this.setState({
            contextMenuOpen: true,
            contextMenuCoords: {x: eventData.pageX, y: eventData.pageY}
        })
    };

    // Close the menu if the user clicks outside the context menu or presses esc key
    closeContextMenu = (eventData) => {
        eventData.preventDefault();
        if (!eventData.target.classList.contains('contextMenu') || eventData.keyCode === 27) {
            this.setState({
                contextMenuOpen: false,
            })
        }
    };

    render() {
        return (
            <div className="App"
                 onClick={this.closeContextMenu}
            >
                <div
                    className={'clickObject'}
                    onContextMenu={this.openContextMenu}
                >
                    Right Click Me
                </div>
                <ContextMenu contextMenuOpen={this.state.contextMenuOpen} coords={this.state.contextMenuCoords}/>
            </div>
        );
    }
}

export default App;
